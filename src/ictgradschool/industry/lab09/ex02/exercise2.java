package ictgradschool.industry.lab09.ex02;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class exercise2 {
    private void start() {
        String [] array = { "ONE" , "TWO" , "THREE" , "FOUR" , "FIVE" , "SIX" , "SEVEN" };
        List<String> myList = new ArrayList<>();
        for (int i = 0; i < array.length ; i++) {
            myList.add(array[i]);
            String element = myList.get(i);
            System.out.println(element);

        }

        for (int i = 0; i < myList.size(); i++) {
            String element = myList.remove(i).toLowerCase();
            myList.add(i, element);
            String elementB = myList.get(i);
            System.out.println(elementB);

        }
        for(String element: myList){
            int i = myList.indexOf(element);
            myList.set(i, element.toLowerCase());
            element = element.toLowerCase();
            System.out.println(element);

        }
        Iterator<String> myIterator = myList.iterator();

        while (myIterator.hasNext()) {
            String element =  myIterator.next();
            int i = myList.indexOf(element);
            myList.set(i, element.toLowerCase());
            System.out.println(element);

        }

        }
    public static void main(String[] args) {
        new exercise2().start();
    }

}
